from datetime import datetime
import pytz


def get_current_time(timezone_str="Etc/GMT-10"):
    """Получить текущее время в системе по указанному часовому поясу
    библиотеки pytz.

    Parameters
    ----------
    timezone_str : str
        Строка указывающая нужный часовой пояс из библиотеки pytz.
        Например, timezone_str = 'Etc/GMT-10' для 'GMT-10'.
        Посмотреть список доступных можно в pytz.all_timezones

    Returns
    -------
    datetime.datetime
        Текущее время в системе в часовом поясе timezone_str.
    """
    # получаем текущее время по часовому поясу UTC
    current_time = datetime.utcnow()
    current_time = current_time.replace(tzinfo=pytz.UTC)

    # переводим его под наш часовой пояс
    tz = pytz.timezone(timezone_str)
    current_time_loc = current_time.astimezone(tz)

    return current_time_loc
